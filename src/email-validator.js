const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export function validate(email) {
    return VALID_EMAIL_ENDINGS.map((ending) => email.endsWith(ending)).includes(true) // eslint-disable-line max-len
};

export function saveEmail(email) {
    localStorage.setItem('subscriptionEmail', email);
};

export function toggleUIState(isSubscribed) {
    if (isSubscribed) {
      document.getElementById('email-getter').style.display = 'none';
      document.getElementById('subscribe-button').textContent = 'Unsubscribe';
    } 
    else {
      document.getElementById('email-getter').style.display = 'block';
      document.getElementById('subscribe-button').textContent = 'Subscribe';
    }
  };


