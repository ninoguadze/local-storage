module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
  },
  'extends': 'google',
  'overrides': [
    {
      'env': {
        'node': true,
      },
      'files': [
        '.eslintrc.{js,cjs}',
      ],
      'parserOptions': {
        'sourceType': 'script',
      },
    },
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module',
  },
  'rules': {
    "no-unused-expressions": "off",
    "linebreak-style": 0,
    "semi": "off",
    "require-jsdoc" : 1,
    "quotes": 1,
    "object-curly-spacing": 0,
    "indent":0,
    "prefer-const": 1,
    "arrow-parens": 1,
    "no-trailing-spaces":0
  },
};
